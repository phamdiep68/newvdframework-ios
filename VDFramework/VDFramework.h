//
//  VDFramework.h
//  VDFramework
//
//  Created by Pham Diep on 11/13/18.
//  Copyright © 2018 Pham Diep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VDConfigNotification.h"

//! Project version number for VDFramework.
FOUNDATION_EXPORT double VDFrameworkVersionNumber;

//! Project version string for VDFramework.
FOUNDATION_EXPORT const unsigned char VDFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VDFramework/PublicHeader.h>


