//
//  VDConfigNotification.h
//  PPCLINK
//
//  Created by Do Lam on 10/8/12.
//
//

/****************************************************************
 * Last updated: 2017.01.06
 *
 *****************************************************************/


#import "VDConfigNotification.h"
#import "CJSONDeserializer.h"
#import <sys/xattr.h>

#if __VDFRAMEWORKSAMPLE___
//#import "RNEncryptor.h"
//#import "RNDecryptor.h"
//#import "Base64.h"
#endif

#define SERVER_CONFIGNOTIFICATION   @"deltago.com"

#define LAST_CODE    @"1963"

//= 1/0 để bật tắt chế độ hiện ngẫu nhiên/tuần tự các notification, default là tuần tự
#define SHOW_NOTIFICATION_RANDOM        0

#define documentPath			[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define libraryPath				[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define appDataDirectoryPath	[libraryPath stringByAppendingString:@"/AppData"]
#define urlDownloadAppIconOnServer   @"http://deltago.com/notifications/adv/public/upload/images"

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if( p != nil ){ [p release]; p = nil; }
#endif

#ifndef SAFE_ENDTIMER
#define SAFE_ENDTIMER(p) if( p != nil ){ [p invalidate]; p = nil; }
#endif

VDConfigNotification* g_vdConfigNotification = nil;

#define fileNameNotificationData                    @"vdnotification.dat"
#define fileNameAdClickCount                        @"vdadclickcount.dat"
#define fileNameAdImpressionCount                   @"vdadimpressioncount.dat"

// File store data generate by user or some server settings...
#define fileNameConfigSettings                      @"vdconfigsettings.dat"

#define CLEVERNET_AD_ID_1   @"89"
#define CLEVERNET_AD_ID_2   @"90"

#define keyExpiredDateAdFree            @"ExpiredDateAdFree"
#define keyNumberOfAdFreeHoursRemain    @"NumberOfAdFreeHoursRemain"

// FOR PPCLINK Reward module
#define REWARD_POINT_SYMBOL                     @"Coin"
#define kRewardCurrentPoint                     @"rewardCurrentPoint"
#define keyRewardInstalledAppIDs                  @"rewardInstalledAppIDs"
#define keyRewardClickedDownloadAppsInfo          @"rewardClickedDownloadAppsInfo"
#define keyCurrentDate                           @"keyCurrentDate"

//@implementation NSURLRequest(DataController)
//+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
//{
//    return YES;
//}
//@end

@interface VDConfigNotification()
{
    //NSMutableData       *_dataReceivedUDID;
    NSMutableData       *_dataReceived;
    NSMutableData       *_dataReceivedCleverNetAd;
   // NSURLConnection     *_urlConnGetUDID;
    NSURLConnection     *_urlConnGetNotifications;
    NSURLConnection     *_urlConnGetCleverNetAdInfo;
    //NSString            *_sUDID;
    
    NSMutableDictionary         *_dictConfigNotifications;
    UIAlertView                 *_alertNotification;
    UIAlertView                 *_alertShowCleverNetAd;
    UIAlertView                 *_alertNewAppVersion;
    NSMutableDictionary         *_dictAdClickCount;
    NSMutableDictionary         *_dictAdImpressionCount;
    NSArray                     *_notifications;
    NSDictionary                *_config;
    
    NSMutableDictionary         *_dictConfigSettings;
    
    //int                         _nCurIndexAd;
    //int                         _nCurIndexAdHighPriority;
    int                         _nAdRate;
    PRODUCT_TYPE                _productType;
    id<VDConfigNotificationDelegate> delegate;
    
    NSMutableDictionary         *_dictCleverNetAdCachedInfo;
    NSMutableDictionary         *_dictLastCleverNetAdInfo;
    NSString                    *_strCleverNetAdZoneID;
    NSString                    *sTestModeAppID, *sTestModeAppVersion;
    BOOL                        _bIsAutoDownloadAllAdImages;
    
    // PPCLINK Reward module
    UIAlertView                 *alTemp;
    BOOL                        bAppDidEnterForeground;
}
- (int)getAdRate;
//- (void)getUDID;
@end

@implementation VDConfigNotification

@synthesize _config;
@synthesize delegate;
@synthesize _notifications;

#pragma Ultilites methods

#if __VDFRAMEWORKSAMPLE___
//- (NSString*) getDecryptedString:(NSString*)inputString
//{
//    //if (ENCODE_ENABLED != 1)
//     //   return inputString;
//    
//    NSData *base64DecodedData = [inputString base64DecodedData];
//    NSError *decryptionError = nil;
//    NSData *resultData = [RNDecryptor decryptData:base64DecodedData withPassword:[self getMyEncodePass] error:&decryptionError];
//    if (decryptionError)
//    {
//        NSLog([NSString stringWithFormat:@"DECRYPT ERROR: %@", decryptionError.description]);
//        return nil;
//    }
//    NSString* sResult = [[[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding] autorelease];
//    if (!sResult)
//        sResult = [resultData base64EncodedStringWithOptions:0];
//    return sResult;
//}
//
//- (NSString*)getMyEncodePass
//{
//    return [NSString stringWithFormat:@"ppcLink%d%@",[self getFirstCode],LAST_CODE]; //generate pass to force hack
//}
//
//- (int) getFirstCode
//{
//    return 101;
//}
#endif

+ (NSDate*)getCurrentDate
{
    /*
    NSDate* dateCurrent = [[NSUserDefaults standardUserDefaults] objectForKey:keyCurrentDate];
    if (!dateCurrent || [dateCurrent compare:[NSDate date]] == NSOrderedAscending)
    {
        dateCurrent = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:dateCurrent forKey:keyCurrentDate];
    }
    return dateCurrent;
     */
    return [NSDate date];
}

- (NSString*) getProductName
{
	return (NSString*)CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), kCFBundleNameKey);
}

+ (NSString*) getProductBuildVersion
{
	return (NSString*)CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), kCFBundleVersionKey);
}

+ (NSString*)getProductOnAppStoreURL
{
    return [g_vdConfigNotification._config objectForKey:kConfig_AppstoreURL];
}

+ (NSString*)getItunesApplicationID
{
    NSString* sID = [[NSUserDefaults standardUserDefaults] objectForKey:@"iTunesApplicationID"];
    
    if ([sID length] > 0)
        return sID;
    
    NSString* sAppStoreURL = [g_vdConfigNotification._config objectForKey:kConfig_AppstoreURL];
    if (sAppStoreURL && [sAppStoreURL rangeOfString:@"http"].location == 0)
    {
//        http://itunes.apple.com/us/app/viettv-xem-tivi-hd-xem-truyen/id568294744?mt=8&uo=4
        NSRange range1 = [sAppStoreURL rangeOfString:@"/id"];
        NSRange range2 = [sAppStoreURL rangeOfString:@"?"];

        NSString* sID = [sAppStoreURL substringWithRange:NSMakeRange(range1.location+range1.length, range2.location-(range1.location+range1.length))];
        [[NSUserDefaults standardUserDefaults] setObject:sID forKey:@"iTunesApplicationID"];
        
        return sID;
    }
    
    return nil;
        
}

+ (NSString*)getItunesApplicationIDWithAppstoreURL:(NSString*)sAppStoreURL
{
    if (sAppStoreURL && [sAppStoreURL rangeOfString:@"http"].location == 0)
    {
        //        http://itunes.apple.com/us/app/viettv-xem-tivi-hd-xem-truyen/id568294744?mt=8&uo=4
        NSRange range1 = [sAppStoreURL rangeOfString:@"/id"];
        NSRange range2 = [sAppStoreURL rangeOfString:@"?"];
        if (range1.length == 0 || range2.length == 0) {
            return nil;
        }
        NSString* sID = [sAppStoreURL substringWithRange:NSMakeRange(range1.location+range1.length, range2.location-(range1.location+range1.length))];
        return sID;
    }
    
    return nil;
    
}

- (NSString*) getProductVersion
{
    if (sTestModeAppVersion)
        return sTestModeAppVersion;
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleShortVersionString"];

	return version;
}


- (NSString*)getBundleID
{
    if (sTestModeAppID)
        return sTestModeAppID;
	return (NSString*)CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), kCFBundleIdentifierKey);
}

+ (int)getRandomIntValue:(int)nN
{
	//srand (time(NULL));
	//return (rand() % nN);
    return arc4random_uniform(nN);
}

+ (int)getRandomIntValue:(int)nN notEqual:(int)nPrev
{
    int nRandom = arc4random_uniform(nN);
    while (nRandom == nPrev)
        nRandom = arc4random_uniform(nN);
    return nRandom;
}
+ (NSData*) downloadContentFromURL:(NSString*)sURL
{
    int nTimeOut = 10;
    NSURLRequest *pTheRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:sURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:nTimeOut];
    if (!pTheRequest) return nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:pTheRequest returningResponse:nil error:nil];
    return returnData;
}

+ (BOOL) downLoadFileFromURL:(NSString*)sURL toFolderPath:(NSString*) sFolderPath withFileName:(NSString*) sFileName:(BOOL) isOverWritten
{
    // Create folder to store file if not exist
    [[NSFileManager defaultManager] createDirectoryAtPath:sFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSString* sDesFilePath = nil;
    if ([sFileName length] > 0)
        sDesFilePath = [NSString stringWithFormat:@"%@/%@", sFolderPath, sFileName];
    else
        sDesFilePath = [NSString stringWithFormat:@"%@/%@", sFolderPath, [sURL lastPathComponent]];
    
    // Not overwritten file if exist
    if ([[NSFileManager defaultManager] fileExistsAtPath:sDesFilePath] &&
        !isOverWritten) return YES;
    
    NSData *returnData = [self downloadContentFromURL:sURL];
    if (!returnData) return NO;
    
    // Check file not found on server error
    NSString* sData = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([sData length] > 0 && ([sData rangeOfString:@"Not Found" options:NSCaseInsensitiveSearch].location != NSNotFound ||
                               [sData rangeOfString:@"Error 404" options:NSCaseInsensitiveSearch].location != NSNotFound))
    {
        //NSLog(@"404 Not Found");
        [sData release];
        return NO;
    }
    
    if ([sData length] > 0 && [sData rangeOfString:@"not found" options:NSCaseInsensitiveSearch].location != NSNotFound)
    {
        //NSLog(@"not found");
        [sData release];
        return NO;
    }
    
    [sData release];
    
    if (![[NSFileManager defaultManager] createFileAtPath:sDesFilePath contents:returnData attributes:nil])
        return NO;
    
    //NSLog(@"Downloaded file OK");
    return YES;
    
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    const char* filePath = [[URL path] fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    if (&NSURLIsExcludedFromBackupKey == nil) {
        // iOS 5.0.1 and lower
        u_int8_t attrValue = 1;
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    } else {
        // First try and remove the extended attribute if it is present
        int result = getxattr(filePath, attrName, NULL, sizeof(u_int8_t), 0, 0);
        if (result != -1) {
            // The attribute exists, we need to remove it
            int removeResult = removexattr(filePath, attrName, 0);
            if (removeResult == 0) {
                //NSLog(@"Removed extended attribute on file %@", URL);
            }
        }
        
        // Set the new key
        return [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
    }
}

-(id)init
{
    if (self = [super init])
    {
#if __VDFRAMEWORKSAMPLE___
//        NSString *sResult = [self getDecryptedString:@"AwHOl3IVQcbfkL6xvSdqOJydYlokiRVw4wiiPzz0kP4SgGrj6jPXm9yRN14MAVmGqNBxv9L9UtP/fAjT+agMt3pDU6YpqDDq0eKdGsE6dQzy9eM4qhWVmdU7afbtBy1Mg/lPLmjt3aNG+NjSlT6CWXuH"];
//        NSLog(sResult);
#endif
        
        _productType = PT_FREE;
        _shouldAutoShowPopupAdWhenOpenApp = YES;
        _bIsAutoDownloadAllAdImages = NO;
        bAppDidEnterForeground = YES;
        
        [[NSFileManager defaultManager] createDirectoryAtPath:appDataDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil];
        
        _dictConfigSettings = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameConfigSettings]];
        if (!_dictConfigSettings)
            _dictConfigSettings = [[NSMutableDictionary alloc] init];
        
//        NSString *sUDID = [_dictConfigSettings objectForKey:keyUDID];
//        if (!sUDID)
//            [self getUDID];
        
        _dictAdClickCount = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameAdClickCount]];
        if (!_dictAdClickCount)
            _dictAdClickCount = [[NSMutableDictionary alloc] init];
        
        _dictAdImpressionCount = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameAdImpressionCount]];
        if (!_dictAdImpressionCount)
            _dictAdImpressionCount = [[NSMutableDictionary alloc] init];
        
        _dictConfigNotifications = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameNotificationData]];
        if (!_dictConfigNotifications)
        {
            _dictConfigNotifications = [[NSMutableDictionary alloc] init];
        // Khởi tạo một số giá trị mặc định của config cho lần đầu tiên
            NSDictionary* dictDefaultConfig = [NSDictionary dictionaryWithObjectsAndKeys:
                                                [NSNumber numberWithInt:4], kConfig_FreeAdsRate,
                                                [NSNumber numberWithInt:4], kConfig_MinFreeAdsRate,
                                                [NSNumber numberWithInt:0], kConfig_PaidAdsRate,
                                                [NSNumber numberWithInt:0], kConfig_MinPaidAdsRate,
                                                @"admob#mopub", kConfig_PPCLINKAdsMediationBanner,
                                                @"admob#mopub", kConfig_PPCLINKAdsMediationInterstitial,
                                                @"admob#mopub", kConfig_PPCLINKAdsMediationVideoInterstitial,
                                                @"facebook#mopub", kConfig_PPCLINKAdsMediationNativeAd,
                                                [NSNumber numberWithInt:240], kConfig_InterstitialAdFreeMinTimeInterval,
                                                [NSNumber numberWithInt:300], kConfig_InterstitialAdFreeMinTimeIntervalBonus,
                                                [NSNumber numberWithInt:18], kConfig_InterstitialAdFreeWhenOpenAppMinTimeInterval,
                                                [NSNumber numberWithInt:50], kConfig_PercentVideoInterstitalAd,
                                                [NSNumber numberWithInt:1], @"ShowBannerAdOnFullscreenLandscapeMode",
                                                [NSNumber numberWithInt:1], @"ShowPopularYoutubeVideosOnHomePage",
                                                @"VN,IN,ID,MY,TH", @"TVCountryCodeAllowParseYoutubeUsingXCD",
                                                @"1000,23,24,25,29,31,32,21,26", @"TVMenuHiddenItems",
                                                @"1000", @"TVMenuHiddenItemsUsingOTAndUD",
                                                [NSNumber numberWithInt:10], @"TVMinOpenTimes",
                                                [NSNumber numberWithInt:5], @"TVMinUsingDays",
                                                [NSNumber numberWithInt:0], @"TVModeParseYoutubeLink",
                                                nil];
            [_dictConfigNotifications setObject:dictDefaultConfig forKey:kConfig];
        }
        _config = [_dictConfigNotifications objectForKey:kConfig];
        _notifications = [_dictConfigNotifications objectForKey:kNotifications];
    
        NSLog(@"CONFIG_NOTIFICATION: %@",_dictConfigNotifications);
        
////: Check if current version is the last version, otherwise prevent app from do recording function
        BOOL bIsMatchTheLatestVersionOnServer = NO;
        NSString* sAppLatestVersion = [_config objectForKey:kConfig_AppLatestVersion];
        if (sAppLatestVersion && [[self getProductVersion] compare:sAppLatestVersion] != NSOrderedDescending)
            bIsMatchTheLatestVersionOnServer = YES;
        
        if (!bIsMatchTheLatestVersionOnServer)
            _notifications = nil;
        
        _dictCleverNetAdCachedInfo = [[NSMutableDictionary alloc] init];
        _dictLastCleverNetAdInfo = [[NSMutableDictionary alloc] init];
        
        // Se update tu ngoai....
        //[[NSNotificationCenter defaultCenter] postNotificationName:kNotifyDidLoadNewConfigNotification object:nil];
        
        /*
        _nCurIndexAd = [[_dictConfigSettings objectForKey:kCurrentIndexAd] intValue];
        if (_nCurIndexAd >= [_notifications count])
            _nCurIndexAd = 0;
        _nCurIndexAdHighPriority = [[_dictConfigSettings objectForKey:kCurrentIndexAdHighPriority] intValue];
        if (_nCurIndexAdHighPriority >= [_notifications count])
            _nCurIndexAdHighPriority = 0;
        */
        
        
        _nAdRate = [self getAdRate];
        
        _strCleverNetAdZoneID = nil;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        //skipBackupDataFromiCloud
        NSURL* url = [NSURL fileURLWithPath:appDataDirectoryPath];
        BOOL bSkipFileResult = NO;
        bSkipFileResult = [self addSkipBackupAttributeToItemAtURL:url];
        
        [self checkSetupURLTypes];
    }
    
    return self;
}

- (void)checkSetupURLTypes
{
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://", [self getBundleID]]]])
    {
        alTemp =[[UIAlertView alloc] initWithTitle:@"VDFramework" message:[NSString stringWithFormat:@"You must add URL Types (Identifier = URL schemes = %@) to detect app installed. Thank you!", [self getBundleID]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alTemp.tag = 789;
        [alTemp show];
    }
}

- (id)initWithProductType:(PRODUCT_TYPE)productType
{
    if (self = [self init])
    {
        [self setProductType:productType];
    }
    return self;
}

- (id)initWithAppID:(NSString*)sAppID andAppVersion:(NSString*)sAppVersion
{
    sTestModeAppVersion = [sAppVersion retain];
    sTestModeAppID = [sAppID retain];
    if (self = [self init])
    {
    }
    return self;
}

- (void)setProductType:(PRODUCT_TYPE)productType
{
    if (_productType != productType)
    {
        _productType = productType;
        _nAdRate = [self getAdRate];
    }
    
}

- (void) setCleverNetAdZoneID:(NSString*)zoneID
{
    [_strCleverNetAdZoneID release];
    _strCleverNetAdZoneID = [[NSString alloc] initWithFormat:@"%@",zoneID];
}


- (int)getAdRate
{
    if (!_config)
        return -1;
    
    int nMaxAdRate, nMinAdRate;
    if (_productType == PT_PAID)
    {
        nMinAdRate = [[_config objectForKey:kConfig_MinPaidAdsRate] intValue];
        nMaxAdRate = [[_config objectForKey:kConfig_PaidAdsRate] intValue];
    }
    else
    {
        nMinAdRate = [[_config objectForKey:kConfig_MinFreeAdsRate] intValue];
        nMaxAdRate = [[_config objectForKey:kConfig_FreeAdsRate] intValue];
    }
    
    if (nMaxAdRate == 0)
        return 0;
    
    return nMinAdRate + [VDConfigNotification getRandomIntValue:(nMaxAdRate-nMinAdRate+1)];
}

- (void)appWillEnterForeground
{
    bAppDidEnterForeground = YES;
}

- (void)appDidBecomeActive
{
    if (!bAppDidEnterForeground)
        return;
    bAppDidEnterForeground = NO;
    
    int nCountOpenApp = [[[NSUserDefaults standardUserDefaults] objectForKey:kCountOpenApp] intValue]; //[[_dictConfigSettings objectForKey:kCountOpenApp] intValue];
    nCountOpenApp++;
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:nCountOpenApp] forKey:kCountOpenApp];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
// Check app update version...
    NSString* sCurAppVersion = [self getProductVersion];
    NSString* sAppVersionOnFile = [NSString stringWithContentsOfFile:[appDataDirectoryPath stringByAppendingString:@"/appversion.dat"] encoding:NSUTF8StringEncoding error:nil];
   
    if (!sAppVersionOnFile || [sCurAppVersion compare:sAppVersionOnFile] == NSOrderedDescending)
    {
        [sCurAppVersion writeToFile:[appDataDirectoryPath stringByAppendingString:@"/appversion.dat"] atomically:NO encoding:NSUTF8StringEncoding error:nil];
        int nUpdate = sAppVersionOnFile ? 1:0; // Check user update version or new installation
        
        NSString* sURL = [NSString stringWithFormat:@"http://deltago.com/notifications/update_app_version.php?appid=%@&version=%@&update=%d", [self getBundleID], sCurAppVersion, nUpdate];
        sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:sURL]
                                                                  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                              timeoutInterval:8.0];
        NSURLConnection *urlConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
    }
    
    [self checkAppInstalledForReward];
    
    [self getNotification];
    
// Luon hien msg tu admin (neu co) ngay moi lan mo app
    BOOL bJustShowAdminMsg = [self showPopUpNotification:NTYPE_MESSAGE forceShow:YES];
    
    if (!bJustShowAdminMsg) // Incase Admin message popup has just shown, dont show popup again
    {
        if (_shouldAutoShowPopupAdWhenOpenApp)
            [self showPopUpNotification:NTYPE_ALL forceShow:NO];
    }
}

- (void)appDidEnterBackground
{
    if (_urlConnGetNotifications)
        [_urlConnGetNotifications cancel];
}

- (void)getNotification
{
//    NSString* sUDID = [_dictConfigSettings objectForKey:keyUDID];
//    if (!sUDID)
//    {
//        if (!_urlConnGetUDID)
//            [self getUDID];
//        return;
//    }
    DEVICE_TARGET curDeviceType = DT_IPHONE;
    
    //if ([[UIDevice currentDevice] deviceFamily] == UIDeviceFamilyiPad)
    if ([[[UIDevice currentDevice] model] hasPrefix:@"iPad"])
        curDeviceType = DT_IPAD;
    
    NSString* sURL = [NSString stringWithFormat:@"http://%@/notifications/get_notifications.php?appid=%@&version=%@&vconfig=%@&target=%d&product=%d&devicename=%@&os=iOS%@", SERVER_CONFIGNOTIFICATION, [self getBundleID], [self getProductVersion], BUILD_VERSION, (int)curDeviceType, (int)_productType, [[UIDevice currentDevice] modelName], [[UIDevice currentDevice] systemVersion]];
    
    sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",sURL);
    
    //sURL = @"http://mdcgate.com/viettv/get_tvplus_list.php";
    
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:sURL]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:15.0];

    _urlConnGetNotifications = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
	if (_urlConnGetNotifications)
		_dataReceived = [[NSMutableData data] retain];
    
    
    /*
     VD ket qua tra ve:
     notifications =     (
     {
     AppId = appid;
     CancelText = Cancel;
     Description = "CleverNET.Ad1";
     MaxClick = 100;
     MaxImpression = 1000;
     NotificationId = 89;
     Title = "CleverNET.Ad1";
     TryNowText = Free;
     Type = 2;
     URL = "http://deltago.com/notifications/getCleverNETAd.php";
     },
     {
     AppId = appid;
     CancelText = Cancel;
     Description = "CleverNET.Ad2";
     MaxClick = 100;
     MaxImpression = 1000;
     NotificationId = 90;
     Title = "CleverNET.Ad2";
     TryNowText = Free;
     Type = 2;
     URL = "http://deltago.com/notifications/getCleverNETAd.php";
     }
     );   
    */
}

//- (void)getUDID
//{
//    NSString *sDeviceDescription = [NSString stringWithFormat:@"%@-iOS%@", [[UIDevice currentDevice] modelName], [[UIDevice currentDevice] systemVersion]];
//    
//    NSString* sURL = [NSString stringWithFormat:@"http://%@/notifications/get_udid.php?appid=%@&appname=%@&version=%@&name=%@", SERVER_CONFIGNOTIFICATION, [self getBundleID], [self getProductName], [self getProductVersion], sDeviceDescription];
//    sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:sURL]
//                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
//                                            timeoutInterval:15.0];
//    
//    _urlConnGetUDID = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
//	if (_urlConnGetUDID)
//		_dataReceivedUDID = [[NSMutableData data] retain];
//}

- (NSString*) getUserAgentString
{
    UIWebView* webView = [[[UIWebView alloc] initWithFrame:CGRectZero] autorelease];
    return [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
}

- (NSString*) getUUIDString
{
    NSString *UUID = nil;
    UUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"kApplicationUUIDKey"];
    if (UUID == nil)
    {
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        UUID = (NSString *)CFUUIDCreateString(NULL, uuid);
        CFRelease(uuid);
        
        [[NSUserDefaults standardUserDefaults] setObject:UUID forKey:@"kApplicationUUIDKey"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return UUID;
}

+ (BOOL)isInstalledAppId:(NSString *)sAppId
{
    if ([NSThread isMainThread])
    {
        return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://", sAppId]]];
    }
    else {
        __block BOOL check;
        dispatch_sync(dispatch_get_main_queue(), ^{
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://", sAppId]]])
                check = YES;
            else
                check = NO;
        });
        return check;
    }
}

- (BOOL)showPopUpNotification:(NOTIFICATION_TYPE)notificationType forceShow:(BOOL)bForceShow
{
    if (!bForceShow)
    {
        if (![self isOnTimeToShowANotification])
            return NO;
    }
    
    return [self forceToShowPopUpNotification:notificationType];
}

- (BOOL)isOnTimeToShowANotification
{
    int nCountOpenApp = [[[NSUserDefaults standardUserDefaults] objectForKey:kCountOpenApp] intValue];
    
    if (_nAdRate > 0 && (nCountOpenApp % _nAdRate) == 0)
    {
        nCountOpenApp = 0;
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:nCountOpenApp] forKey:kCountOpenApp];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // Reset adrate with random value
        _nAdRate = [self getAdRate];
        
        return YES;
    }
    else
        return NO;
    
    
}

- (NSDictionary*)getANotificationInfoToShow:(NOTIFICATION_TYPE)notificationType
{
    return [self getANotificationInfoToShow:notificationType forPPCLINKNativeAd:NO];
}

- (NSDictionary*)getANotificationInfoToShow:(NOTIFICATION_TYPE)notificationType forPPCLINKNativeAd:(BOOL)bPPCLINKNativeAd
{
    if ([[_config objectForKey:kConfig_EnableAds] intValue] == 0)
        return nil;
    if ([_notifications count] == 0) return nil;
    
    int nCurIdAd = [[_dictConfigSettings objectForKey:kCurrentIndexAd] intValue];
    if (notificationType == NTYPE_APP_HIGH_PRIORITY)
        nCurIdAd = [[_dictConfigSettings objectForKey:kCurrentIndexAdHighPriority] intValue];
    if (nCurIdAd > [_notifications count])
        nCurIdAd = 0;
    
    // CHẾ ĐỘ TUẦN TỰ
    if (SHOW_NOTIFICATION_RANDOM)
    {
        // Khởi tạo index notification ngẫu nhiên thay vì tuần tự
        nCurIdAd = [VDConfigNotification getRandomIntValue:[_notifications count] notEqual:nCurIdAd];
    }
    else
    {
        nCurIdAd++;
        if (nCurIdAd >= [_notifications count])
            nCurIdAd = 0;
    }
    
    NSString* sCurAdID = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_ID];
    int nCurAdMaxClick = [[[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_MaxClick] intValue];
    int nCurAdMaxImpression = [[[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotifcation_MaxImpression] intValue];
    
    NOTIFICATION_TYPE curNotifyType = (NOTIFICATION_TYPE)[[[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_Type] intValue];
    NSString *sCurAdAppId = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_AppId];
    NSString *sCurAppTitle = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_Title];
    NSString *sCurAdRectangleImage = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_AdRectangle];
    NSString *sCurTryNowText = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_TryNowText];
    NSString *sAppBundleID = [self getBundleID];
    
    int nStartIndexID = nCurIdAd;
    
    while ((notificationType != NTYPE_ALL && notificationType != curNotifyType) ||
           ((curNotifyType == NTYPE_APP || curNotifyType == NTYPE_APP_HIGH_PRIORITY || curNotifyType == NTYPE_INAPP || curNotifyType == NTYPE_MESSAGE) && ([[_dictAdClickCount objectForKey:sCurAdID] intValue] >= nCurAdMaxClick || [[_dictAdImpressionCount objectForKey:sCurAdID] intValue] >= nCurAdMaxImpression)) ||
           ((curNotifyType == NTYPE_APP || curNotifyType == NTYPE_APP_HIGH_PRIORITY) && ([sAppBundleID isEqualToString:sCurAdAppId] ||
                                                                                         [VDConfigNotification isInstalledAppId:sCurAdAppId] // prevent app ad itself and app that is installed
                                                                                         )) || [sCurAppTitle isEqualToString:@"na"] // Title ko hop le hoac ko muon hien kieu popup (chi muon hien rectangle/full
                                                                || (bPPCLINKNativeAd && [sCurAdRectangleImage length] < 2) // Neu la get for PPCLINKAd native thi phai dam bao toi thieu co anh cover (AdRectangle)
                                                                || ([sCurTryNowText length] < 2)) // Phai co TryNowText moi show dc popup
           
    {
        //NSLog(sCurAppTitle);
        nCurIdAd++;
        if (nCurIdAd >= [_notifications count])
            nCurIdAd = 0;
        
        if (nCurIdAd == nStartIndexID)  // There is nothing to show...
            return nil;
        
        curNotifyType = (NOTIFICATION_TYPE)[[[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_Type] intValue];
        sCurAdID = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_ID];
        nCurAdMaxClick = [[[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_MaxClick] intValue];
        nCurAdMaxImpression = [[[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotifcation_MaxImpression] intValue];
        sCurAdAppId = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_AppId];
        sCurAppTitle = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_Title];
        sCurAdRectangleImage = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_AdRectangle];
        sCurTryNowText = [[_notifications objectAtIndex:nCurIdAd] objectForKey:kNotification_TryNowText];
        
    }
    
    // Cap nhat lai index hien tai cua quang cao
    if (curNotifyType == NTYPE_APP_HIGH_PRIORITY)
        [_dictConfigSettings setObject:[NSNumber numberWithInt:nCurIdAd] forKey:kCurrentIndexAdHighPriority];
    else
        [_dictConfigSettings setObject:[NSNumber numberWithInt:nCurIdAd] forKey:kCurrentIndexAd];
    [_dictConfigSettings writeToFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameConfigSettings] atomically:NO];
    
    NSMutableDictionary* dictNotifyInfo = [[NSMutableDictionary alloc] initWithDictionary:[_notifications objectAtIndex:nCurIdAd]];
    
    if (curNotifyType == NTYPE_APP || curNotifyType == NTYPE_APP_HIGH_PRIORITY || curNotifyType == NTYPE_VIEWADS)
    {
        int nCostPerDay = [[_config objectForKey:kConfig_CostOneDayFreeAd] intValue];
        if (nCostPerDay <= 0) // TH không config hoặc giá trị âm, set lại mặc định bằng 1
            nCostPerDay = 1;
        
        // Kiểm tra xem app đã từng cài để nhận thưởng chưa, nếu rồi thì sẽ không được nhận thưởng nữa!
        NSArray* installedAppIDs = [[NSUserDefaults standardUserDefaults] objectForKey:keyRewardInstalledAppIDs];
        if ([installedAppIDs containsObject:[dictNotifyInfo objectForKey:kNotification_AppId]])
            [dictNotifyInfo setObject:[NSNumber numberWithInt:0] forKey:kNotification_InstalledRewardPoints];
        
        int nNumberOfAdFreeHours = [[dictNotifyInfo objectForKey:kNotification_InstalledRewardPoints] intValue]/nCostPerDay ;
        [dictNotifyInfo setObject:[NSNumber numberWithInt:nNumberOfAdFreeHours] forKey:kNotification_NumberOfAdFreeHours];
    }
    
    if (curNotifyType == NTYPE_APP || curNotifyType == NTYPE_APP_HIGH_PRIORITY)
    {
        NSString* sImgFullScreenAd = [dictNotifyInfo objectForKey:kNotification_AdFullscreen];
        if ([sImgFullScreenAd rangeOfString:@"http://"].location == NSNotFound)
        {
            // Replace AdImage Name by AdImage URL on server for download
            sImgFullScreenAd = [NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,sImgFullScreenAd];
            [dictNotifyInfo setObject:sImgFullScreenAd forKey:kNotification_AdFullscreen];
        }
    }
    
//    if ([[dictNotifyInfo objectForKey:kNotification_TryNowText] length] == 0)
//        [dictNotifyInfo setObject:@"Try now" forKey:kNotification_TryNowText];
    if ([[dictNotifyInfo objectForKey:kNotification_CancelText] length] == 0)
        [dictNotifyInfo setObject:@"Not now" forKey:kNotification_CancelText];
    
    return [dictNotifyInfo autorelease];
}


- (NSDictionary*)getAPPCLINKNativeAd
{
    NSDictionary* dictCurNotifyInfo = nil;
    NSString *sAdIconURL = nil;
    NSString *sAdRectangleURL = nil;
    
    dictCurNotifyInfo = [g_vdConfigNotification getANotificationInfoToShow:NTYPE_APP_HIGH_PRIORITY forPPCLINKNativeAd:YES];
    if (!dictCurNotifyInfo)
        dictCurNotifyInfo = [g_vdConfigNotification getANotificationInfoToShow:NTYPE_APP forPPCLINKNativeAd:YES];
    if (!dictCurNotifyInfo)
        return nil;
    
    // Dam bao lay dc notification (native ad) co anh cover
    sAdRectangleURL = [dictCurNotifyInfo objectForKey:kNotification_AdRectangle];  // Cover image
    if ([sAdRectangleURL length] < 2)
        return nil;

    NSMutableDictionary *dictPPCLINKNativeAd = [[NSMutableDictionary alloc] initWithDictionary:dictCurNotifyInfo];
    
// AdRectangle full URL
    if ([sAdRectangleURL rangeOfString:@"http://"].location == NSNotFound)
    {
        [dictPPCLINKNativeAd setObject:[NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,sAdRectangleURL] forKey:kNotification_AdRectangle];
    }
// AdIcon full URL
    sAdIconURL = [dictCurNotifyInfo objectForKey:kNotification_AdImage];
    if ([sAdIconURL length] > 1)
     if ([sAdIconURL rangeOfString:@"http://"].location == NSNotFound)
        {
            [dictPPCLINKNativeAd setObject:[NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,sAdIconURL] forKey:kNotification_AdImage];
        }
    
    return [dictPPCLINKNativeAd autorelease];

}

- (BOOL) forceToShowPopUpNotification:(NOTIFICATION_TYPE)notificationType
{
    if (_alertNotification)
        return NO;
    
// Neu la ban Pro hoac dang trong thoi gian free quang cao thi chi uu tien hien loai NTYPE_MESSAGE de truong hop co message can thong bao gap tu admin
    if (_productType == PT_PAID || [self isInAdFreeTime])
    {
        notificationType = NTYPE_MESSAGE;
        //return NO;
    }
    
    NSDictionary* dictNotifyInfo = nil;
    if (notificationType != NTYPE_MESSAGE)  // Neu ko phai msg tu admin (uu tien cao nhat) thi uu tien app NTYPE_APP_HIGH_PRIORITY
        [self getANotificationInfoToShow:NTYPE_APP_HIGH_PRIORITY];
    
    if (!dictNotifyInfo)
        dictNotifyInfo = [self getANotificationInfoToShow:notificationType];
    
    if (!dictNotifyInfo)
        return NO;
   
    NOTIFICATION_TYPE curNotifyType = (NOTIFICATION_TYPE)[[dictNotifyInfo objectForKey:kNotification_Type] intValue];
    
    int nCurIdAd = [[_dictConfigSettings objectForKey:kCurrentIndexAd] intValue];
    if (curNotifyType == NTYPE_APP_HIGH_PRIORITY)
        nCurIdAd = [[_dictConfigSettings objectForKey:kCurrentIndexAdHighPriority] intValue];
    
    // Show ad
    if (curNotifyType == NTYPE_CLEVERNET)
    {
        /* Hien thi quang cao clevernet theo trinh tu:
         1. Request quang cao CleverNET den Sever
         2. Neu nhan duoc thanh cong thi hien thi ket qua nhan duoc
         3. Neu request that bai (hoac ket qua ko hop le), tim va hien thi quang cao non-CleverNET tiep theo
         */
        /* Clear cached info */
        [_dictCleverNetAdCachedInfo setObject:[NSNumber numberWithBool:NO] forKey:kCleverNetAdInfo_Ready];
        
        /* Request Info for the next cleverNetAd show */
        if (!_strCleverNetAdZoneID)
            _strCleverNetAdZoneID = [[NSString alloc] initWithString:CLEVERNET_AD_TEXT_ZONE_ID];
        NSString *strUUID = [self getUUIDString];
        NSString *strUserAgent = [self getUserAgentString];
        NSString *strDeviceName = [[UIDevice currentDevice] name];
        NSString *strIsIOS = @"IOS";
        
        NSString* sURL = [NSString stringWithFormat:@"http://deltago.com/notifications/getCleverNETAd.php?zoneid=%@&imeinumber=%@&DeviceName=%@&OS=%@&ua=%@",_strCleverNetAdZoneID,strUUID,strDeviceName,strIsIOS,strUserAgent];
        sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"%@",sURL);
        
        NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:sURL]
                                                    cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                timeoutInterval:15.0];
        
        _urlConnGetCleverNetAdInfo = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        if (_urlConnGetCleverNetAdInfo)
            _dataReceivedCleverNetAd = [[NSMutableData data] retain];
        
    }
    else
    {
        if (curNotifyType != NTYPE_MESSAGE && [delegate respondsToSelector:@selector(VDConfigNotification:showNotificationWithInfo:)])
            [delegate VDConfigNotification:self showNotificationWithInfo:dictNotifyInfo];
        else
        {
            NSString *sTryNowText = [dictNotifyInfo objectForKey:kNotification_TryNowText];
            // Trường hợp thông báo đơn thuần, không gợi ý user click view để xem thêm gì cả thì chỉ cần 1 nut Cancel text
            if (curNotifyType == NTYPE_MESSAGE && [[dictNotifyInfo objectForKey:kNotification_URL] length] < 2)
                sTryNowText = nil;
            
            _alertNotification = [[UIAlertView alloc] initWithTitle:[dictNotifyInfo objectForKey:kNotification_Title] message:[dictNotifyInfo objectForKey:kNotification_Description] delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:sTryNowText, nil];
            _alertNotification.tag = nCurIdAd;
            [_alertNotification show];
            [_alertNotification release];
        }
        
        [self justShowANotifyWithNotificationId:[dictNotifyInfo objectForKey:kNotification_ID]];
        
//        NSDate* interstitialAdFreeExpiredDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"interstitialAdFreeExpiredDate"];
//        NSLog(@"%@", interstitialAdFreeExpiredDate);
//        
        if (curNotifyType == NTYPE_APP || curNotifyType == NTYPE_APP_HIGH_PRIORITY)
        {
            int nTimeInterval = [[_config objectForKey:kConfig_InterstitialAdFreeMinTimeInterval] intValue];
            [[NSUserDefaults standardUserDefaults] setObject:[[VDConfigNotification getCurrentDate] dateByAddingTimeInterval:nTimeInterval] forKey:@"interstitialAdFreeExpiredDate"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
//        interstitialAdFreeExpiredDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"interstitialAdFreeExpiredDate"];
//        NSLog(@"%@", interstitialAdFreeExpiredDate);
    }
    
    return YES;
}


- (void)justShowNotificationWithID:(NSString *)sNotificationID adImageType:(ADIMAGE_TYPE)adImageType
{
    // Count impression of current ad
    int nCurAdImpressionCount = [[_dictAdImpressionCount objectForKey:sNotificationID] intValue];
    [_dictAdImpressionCount setObject:[NSNumber numberWithInt:nCurAdImpressionCount + 1] forKey:sNotificationID];
    [_dictAdImpressionCount writeToFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameAdImpressionCount] atomically:NO];
    
    // Impression count to server
    NSString* sURL = [NSString stringWithFormat:@"http://%@/notifications/handle_click.php?appid=%@&id=%@&adimagetype=%d&impression=1", SERVER_CONFIGNOTIFICATION, [self getBundleID], sNotificationID, (int)adImageType];
    sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Count impression view to %@", sURL);
    
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:sURL]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:13.0];
    NSURLConnection *urlConnClickReport = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
}

- (void)justShowANotifyWithNotificationId:(NSString *)sNotificationID
{
    [self justShowNotificationWithID:sNotificationID adImageType:ADIMAGETYPE_ICON];
}

- (void)userJustClickTryNowWithNotificationId:(NSString *)sNotificationID adImageType:(ADIMAGE_TYPE)adImageType
{
    // Click count to server
    NSString* sURL = [NSString stringWithFormat:@"http://%@/notifications/handle_click.php?appid=%@&id=%@&adimagetype=%d", SERVER_CONFIGNOTIFICATION, [self getBundleID],sNotificationID, (int)adImageType];
    
    // CLEVERNET Handle
    //if ([sNotificationID isEqualToString:CLEVERNET_AD_ID_1] || [sNotificationID isEqualToString:CLEVERNET_AD_ID_2])
    if (_dictLastCleverNetAdInfo && [self isCleverNETNotificationID:sNotificationID])
    {
        NSString *strExtraInfo = [_dictLastCleverNetAdInfo objectForKey:@"link"];
        NSArray *arr = [strExtraInfo componentsSeparatedByString:@"zoneid="];
        strExtraInfo = [NSString stringWithFormat:@"&zoneid=%@",[arr lastObject]];
        strExtraInfo = [strExtraInfo lowercaseString];
        sURL = [sURL stringByAppendingString:strExtraInfo];
    }
    
    sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Count click view to %@", sURL);
    
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:sURL]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:13.0];
    NSURLConnection *urlConnClickReport = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
    
    int nCurAdClickCount = [[_dictAdClickCount objectForKey:sNotificationID] intValue];
    [_dictAdClickCount setObject:[NSNumber numberWithInt:nCurAdClickCount + 1] forKey:sNotificationID];
    [_dictAdClickCount writeToFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameAdClickCount] atomically:NO];
    

}

- (void)userJustClickTryNowWithNotificationId:(NSString *)sNotificationID
{
    [self userJustClickTryNowWithNotificationId:sNotificationID adImageType:ADIMAGETYPE_ICON];
}


- (void)userJustClickTryNowWithNotificationId:(NSString *)sNotificationID forReward:(BOOL)bYesNo
{
    [self userJustClickTryNowWithNotificationId:sNotificationID];
    
    if (bYesNo) // Incase click view/install app for reward
    {
        
        NSDictionary* dictNotificationInfo = nil;
        for (int i = 0; i < [_notifications count]; i++)
            if ([sNotificationID isEqualToString:[[_notifications objectAtIndex:i] objectForKey:kNotification_ID]])
            {
                dictNotificationInfo = [_notifications objectAtIndex:i];
                break;
            }
        if ([[dictNotificationInfo objectForKey:kNotification_InstalledRewardPoints] intValue] <= 0 ||
            [[dictNotificationInfo objectForKey:kNotification_Type] intValue] != NTYPE_APP)
            return;
        
        /* Click download app for reward handle
         Nếu là loại popup app download để được thưởng ngày ad free
         */
        NSMutableDictionary *clickDownloadAppsInfo = [[[[NSUserDefaults standardUserDefaults] objectForKey:keyRewardClickedDownloadAppsInfo] mutableCopy] autorelease];
        if (!clickDownloadAppsInfo)
            clickDownloadAppsInfo = [[NSMutableDictionary alloc] init];
        
        NSString* sAppID = [dictNotificationInfo objectForKey:kNotification_AppId];
        
        if (![[clickDownloadAppsInfo allKeys] containsObject:sAppID])
            [clickDownloadAppsInfo setObject:dictNotificationInfo forKey:sAppID];
        
        [[NSUserDefaults standardUserDefaults] setObject:clickDownloadAppsInfo forKey:keyRewardClickedDownloadAppsInfo];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    
}

- (BOOL) isCleverNETNotificationID:(NSString*)strID
{
    BOOL isCleverNETNotify = FALSE;
   
    for (int i = 0; i < [_notifications count]; i++)
    {
        NOTIFICATION_TYPE curNotifyType = (NOTIFICATION_TYPE)[[[_notifications objectAtIndex:i] objectForKey:kNotification_Type] intValue];
        NSString* sCurAdID = [[_notifications objectAtIndex:i] objectForKey:kNotification_ID];
        
        if ([sCurAdID isEqualToString:strID] && curNotifyType == NTYPE_CLEVERNET)
            isCleverNETNotify = TRUE;
    }
    
    return isCleverNETNotify;
}


- (NSArray*)getListOfNotificationWithAdImageType:(ADIMAGE_TYPE)adImageType
{
    return [self getListOfNotificationWithAdImageType:adImageType includeInstalledApps:NO];
}

 - (NSArray*)getListOfNotificationWithAdImageType:(ADIMAGE_TYPE)adImageType includeInstalledApps:(BOOL)bIncludeInstalledApps
{
    NSString* sKeyAdImage = nil;
    
    switch (adImageType) {
        case ADIMAGETYPE_ICON:
            sKeyAdImage = kNotification_AdImage;
            break;
        case ADIMAGETYPE_BANNER:
            sKeyAdImage = kNotification_AdBanner;
            break;
        case ADIMAGETYPE_RECTANGLE:
            sKeyAdImage = kNotification_AdRectangle;
            break;
        case ADIMAGETYPE_FULLSCREEN:
            sKeyAdImage = kNotification_AdFullscreen;
            break;
        case ADIMAGE_TYPE_ICONMOREAPPS:
            sKeyAdImage = kNotification_AdIconMoreApps;
            break;
        default:
            sKeyAdImage = nil;
            break;
    }
    
    if (!sKeyAdImage)
    {
        NSLog(@"Invalid ADIMAGE_TYPE!");
        return nil;
    }
    
    NSMutableArray *arrNotification = [[NSMutableArray alloc] init];
    NSMutableDictionary* dictCur = nil;
    NOTIFICATION_TYPE nCurNType;
    NSString* strImage = nil;
    NSString *strAppID = nil;
    int nCurAdOrder = 0;
    int nInsertIndex = 0;
    for (int index = 0; index < _notifications.count; index++)
    {
        if([[_notifications objectAtIndex:index] isKindOfClass:[NSDictionary class]])
        {
            dictCur = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)[_notifications objectAtIndex:index]];
            
            nCurNType = (NOTIFICATION_TYPE)[[dictCur objectForKey:kNotification_Type] intValue];
            strImage = [dictCur objectForKey:sKeyAdImage];
            strAppID = [dictCur objectForKey:kNotification_AppId];
            
            if ((nCurNType == NTYPE_APP || nCurNType == NTYPE_APP_HIGH_PRIORITY) &&
                strImage && strImage != (id)[NSNull null] && strImage.length > 0 &&
                (bIncludeInstalledApps || ![VDConfigNotification isInstalledAppId:strAppID]))
            {
                if ([strImage rangeOfString:@"http://"].location == NSNotFound)
                {
                    // Replace AdImage Name by AdImage URL on server for download
                    strImage = [NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,strImage];
                    [dictCur setObject:strImage forKey:sKeyAdImage];
                }
                
                // Insert object to the right-order
                nCurAdOrder = [[dictCur objectForKey:kNotification_AdOrder] intValue];
                nInsertIndex = 0;
                while (nInsertIndex < [arrNotification count] && nCurAdOrder > [[[arrNotification objectAtIndex:nInsertIndex] objectForKey:kNotification_AdOrder] intValue])
                    nInsertIndex++;
                
                [arrNotification insertObject:dictCur atIndex:nInsertIndex];
                
                [dictCur release];
            }
        }
    }
    return [arrNotification autorelease];
}

- (NSArray*)getListOfNotificationForMoreApps:(BOOL)bIncludeInstalledApps
{
    return [self getListOfNotificationWithAdImageType:ADIMAGETYPE_ICON includeInstalledApps:bIncludeInstalledApps];
}

- (BOOL)isNewVersionInTestOrReviewTime
{
//Giả sử version hiện tại trên store của app la 1.1. Nếu hiện tại version mới là 1.2 đang trong giai đoạn test hoặc đã upload và đang chờ review, chưa có mặt trên AppStore thì hàm sẽ trả về YES. Sau khi được bản 1.2 được approved và có mặt trên store, cần cập nhật lại version của app tương ứng trên server để app có thể mở đầy đủ tính năng.
    NSString* sAppLatestVersionOnServer = nil;
    if (_config != nil) {
        sAppLatestVersionOnServer = [_config objectForKey:kConfig_AppLatestVersion];
        if (sAppLatestVersionOnServer == nil) {
            return YES;
        }
        if ([sAppLatestVersionOnServer compare:[self getProductVersion]] == NSOrderedAscending) {
            return YES;
        } else {
            return NO;
        }
    } else {
        return YES;
    }
    
//    if (sAppLatestVersionOnServer && [[self getProductVersion] compare:sAppLatestVersionOnServer] != NSOrderedDescending) {
//        return YES;
//    } else {
//         return NO;
//    }
    
    
    return YES;
}

#pragma mark - PPCLINK REWARD (Ad Free Bonus) methods
- (NSArray*)getListOfAppDownloadForReward
{
    NSMutableArray *arrAppNotification = [[NSMutableArray alloc] init];
    NSMutableDictionary* dictCur = nil;
    NOTIFICATION_TYPE nCurNType;
    NSString* strImage = nil;
    NSString *strAppID = nil;
    NSString* sKeyAdImage = kNotification_AdImage;
    int nInstalledRewardPoints = 0;
    int nNumberOfAdFreeHours = 0;
    int nCostOneDayFreeAd = [[_config objectForKey:kConfig_CostOneDayFreeAd] intValue];
    if (nCostOneDayFreeAd <= 0) // TH không config hoặc giá trị âm, set lại mặc định bằng 1
        nCostOneDayFreeAd = 1;
    
    NSArray* installedAppIDs = [[NSUserDefaults standardUserDefaults] objectForKey:keyRewardInstalledAppIDs];
    
    for (int index = 0; index < [_notifications count]; index++)
    {
        nCurNType = (NOTIFICATION_TYPE)[[[_notifications objectAtIndex:index] objectForKey:kNotification_Type] intValue];
        strImage = [[_notifications objectAtIndex:index] objectForKey:sKeyAdImage];
        strAppID = [[_notifications objectAtIndex:index] objectForKey:kNotification_AppId];
        nInstalledRewardPoints = [[[_notifications objectAtIndex:index] objectForKey:kNotification_InstalledRewardPoints] intValue];
        nNumberOfAdFreeHours = nInstalledRewardPoints/nCostOneDayFreeAd;
        
        if ((nCurNType == NTYPE_APP) && nInstalledRewardPoints > 0 &&
            strImage && strImage != (id)[NSNull null] && strImage.length > 0 &&   // App icon must-have
            ![VDConfigNotification isInstalledAppId:strAppID] &&  // App currently not install on device
            ![installedAppIDs containsObject:strAppID])  ////App must not installed on device before
        {
            dictCur = [[NSMutableDictionary alloc] initWithDictionary:[_notifications objectAtIndex:index]];
            
            if ([strImage rangeOfString:@"http://"].location == NSNotFound)  //Generate App Icon URL
            {
                // Replace AdImage Name by AdImage URL on server for download
                strImage = [NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,strImage];
                [dictCur setObject:strImage forKey:sKeyAdImage];
            }
            [dictCur setObject:[NSNumber numberWithInt:nNumberOfAdFreeHours] forKey:kNotification_NumberOfAdFreeHours];
            [arrAppNotification addObject:dictCur];
            [dictCur release];
        }
    }
    return [arrAppNotification autorelease];
}

// Hàm này gọi mỗi khi mở app để kiểm tra việc cài đặt app của user để thưởng điểm
- (void)checkAppInstalledForReward
{
    NSString* sCurAppID = nil;
    NSMutableDictionary *clickDownloadAppsInfo = [[[[NSUserDefaults standardUserDefaults] objectForKey:keyRewardClickedDownloadAppsInfo] mutableCopy] autorelease];
    NSMutableArray* installedAppIDs = [[[[NSUserDefaults standardUserDefaults] objectForKey:keyRewardInstalledAppIDs] mutableCopy] autorelease];
    
    for (int i = 0; i < [[clickDownloadAppsInfo allKeys] count]; i++)
     if ([VDConfigNotification isInstalledAppId:[[clickDownloadAppsInfo allKeys] objectAtIndex:i]])
     {
         sCurAppID = [[clickDownloadAppsInfo allKeys] objectAtIndex:i];
         // Fill info to sInstalledAppInfo here...
         
         if (!installedAppIDs)
             installedAppIDs = [[NSMutableArray alloc] init];
         [installedAppIDs addObject:sCurAppID];
         
         NSMutableDictionary* dictCurAppInstalled = [[[clickDownloadAppsInfo objectForKey:sCurAppID] mutableCopy] autorelease];
         
         int nCostPerDay = [[_config objectForKey:kConfig_CostOneDayFreeAd] intValue];
         if (nCostPerDay <= 0) // TH không config hoặc giá trị âm, set lại mặc định bằng 1
             nCostPerDay = 1;
         int nNumberOfAdFreeHours = [[dictCurAppInstalled objectForKey:kNotification_InstalledRewardPoints] intValue]/nCostPerDay;
         
         [dictCurAppInstalled setObject:[NSNumber numberWithInt:nNumberOfAdFreeHours] forKey:kNotification_NumberOfAdFreeHours];
         
         // Notify user complete install app with app info object
         //[[NSNotificationCenter defaultCenter] postNotificationName:kNotifyRewardUserInstalledApp object:dictCurAppInstalled];
         if ([delegate respondsToSelector:@selector(VDConfigNotification:userDidInstallAppForRewardWithInfo:)])
             [delegate VDConfigNotification:self userDidInstallAppForRewardWithInfo:dictCurAppInstalled];
         else
         {
             UIAlertView *al = [[UIAlertView alloc] initWithTitle:[self getProductName] message:[NSString stringWithFormat:@"Bạn được tặng %d ngày không phải xem quảng cáo vì đã cài đặt thành công ứng dụng %@", nNumberOfAdFreeHours/24, [dictCurAppInstalled objectForKey:kNotification_Title]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [al show];
             [al release];
         }
         
         [clickDownloadAppsInfo removeObjectForKey:sCurAppID];
         
         [[NSUserDefaults standardUserDefaults] setObject:clickDownloadAppsInfo forKey:keyRewardClickedDownloadAppsInfo];
         [[NSUserDefaults standardUserDefaults] setObject:installedAppIDs forKey:keyRewardInstalledAppIDs];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         // Break luôn vì thường user chỉ cài từng app một, ko sợ xót vì sẽ được cộng ở lượt mở app sau nếu user cài thành công cùng lúc hơn 1 app
         break;
     }
}


#pragma mark - CleverNET Ad Handle
- (void) didGetCleverNETAdInfoFailed
{
    NSLog(@"CLEVERNET : didGetCleverNETAdInfoFailed");
    // Truong hop chua load duoc quang cao cleverNet thi hien thi mot quang cao PPCLINK thay the
    [self forceToShowPopUpNotification:NTYPE_ALL];
}

- (void) didGetCleverNETAdInfoSuccessfully
{
    NSLog(@"CLEVERNET : didGetCleverNETAdInfoSuccessfully");

    //  Now you are ready to show cleverNet Ad
    if ([[_dictCleverNetAdCachedInfo objectForKey:kCleverNetAdInfo_Ready] boolValue])
    {
        // Hien thi quang cao cleverNet da load duoc va cached lai truoc do
        NSString *strTitle = [_dictCleverNetAdCachedInfo objectForKey:kCleverNetAdCachedInfo_Title];
        NSString *strContent = [_dictCleverNetAdCachedInfo objectForKey:kCleverNetAdCachedInfo_Content];
        NSString *strLink = [_dictCleverNetAdCachedInfo objectForKey:kCleverNetAdCachedInfo_Link];
        [_dictCleverNetAdCachedInfo setObject:strLink forKey:kCleverNetAdCachedInfo_CurrentURL];
        
        int nCurIdAd = [[_dictConfigSettings objectForKey:kCurrentIndexAd] intValue];
        NSMutableDictionary* dictNotifyInfo = [[[NSMutableDictionary alloc] initWithDictionary:[_notifications objectAtIndex:nCurIdAd]] autorelease];
        [dictNotifyInfo setObject:strTitle forKey:kNotification_Title];
        [dictNotifyInfo setObject:strContent forKey:kNotification_Description];
        [dictNotifyInfo setObject:strLink forKey:kNotification_URL];
        
        if ([[dictNotifyInfo objectForKey:kNotification_TryNowText] length] == 0)
            [dictNotifyInfo setObject:@"Try now" forKey:kNotification_TryNowText];
        if ([[dictNotifyInfo objectForKey:kNotification_CancelText] length] == 0)
            [dictNotifyInfo setObject:@"Not now" forKey:kNotification_CancelText];
        
        if ([delegate respondsToSelector:@selector(VDConfigNotification:showNotificationWithInfo:)])
            [delegate VDConfigNotification:self showNotificationWithInfo:dictNotifyInfo];
        else
        {
            _alertShowCleverNetAd = [[UIAlertView alloc] initWithTitle:strTitle message:strContent delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:kNotification_TryNowText, nil];
            _alertShowCleverNetAd.tag = nCurIdAd;
            [_alertShowCleverNetAd show];
            [_alertShowCleverNetAd release];
        }
        
        [_dictCleverNetAdCachedInfo setObject:[NSNumber numberWithBool:NO] forKey:kCleverNetAdInfo_Ready];
        
        [self justShowANotifyWithNotificationId:[dictNotifyInfo objectForKey:kNotification_ID]];
    }
}

#pragma mark - AlertView Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView == alTemp)
    {
        //exit(1);
    }
    if (alertView == _alertNotification)
    {
        switch (buttonIndex)
        {
            case 0: //Cannel
                break;
                
            case 1: // Try Now
            {
                NSInteger nCurIndexAd = alertView.tag;
                if (nCurIndexAd >=0 && nCurIndexAd < [_notifications count])
                {
                    NSString *sNotificationID = [[_notifications objectAtIndex:nCurIndexAd] objectForKey:kNotification_ID];
                    [self userJustClickTryNowWithNotificationId:sNotificationID];
                
                    NSString *sURL = [[_notifications objectAtIndex:nCurIndexAd] objectForKey:kNotification_URL];
                    NSLog(@"Open URL: %@", sURL);
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sURL]];
                }
            
                break;
            }
                
            default:
                break;
        }
        
        _alertNotification = nil;
    }
    else if (alertView == _alertShowCleverNetAd)
    {
        switch (buttonIndex)
        {
            case 0: //Cannel
                break;
                
            case 1: // Try Now
            {
                int nCurIndexAd = alertView.tag;
                NSString *sNotificationID = [[_notifications objectAtIndex:nCurIndexAd] objectForKey:kNotification_ID];
                [self userJustClickTryNowWithNotificationId:sNotificationID];
                
                NSString *sURL = [_dictCleverNetAdCachedInfo objectForKey:kCleverNetAdCachedInfo_CurrentURL];
                NSLog(@"Open URL: %@", sURL);
                sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sURL]];
                                 
                
                break;
            }
                
            default:
                break;
        }
        
        _alertShowCleverNetAd = nil;
    }
    else if (alertView == _alertNewAppVersion)
    {
        switch (buttonIndex)
        {
            case 0: //Cannel
                [_dictConfigSettings setObject:[NSNumber numberWithBool:NO] forKey:[kRemindUpdateNewAppVersion stringByAppendingString:[_config objectForKey:kConfig_AppLatestVersion]]];
                break;
                
            case 1: // Update
            {
                [_dictConfigSettings setObject:[NSNumber numberWithBool:NO] forKey:[kRemindUpdateNewAppVersion stringByAppendingString:[_config objectForKey:kConfig_AppLatestVersion]]];
                
                NSString* sURL = [_config objectForKey:kConfig_AppstoreURL];
                if ([sURL length] > 0)
                {
                    NSLog(@"Open URL: %@", sURL);
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sURL]];
                }
                
                break;
            }
                
            case 2: // Remind me later
                [_dictConfigSettings setObject:[NSNumber numberWithBool:YES] forKey:[kRemindUpdateNewAppVersion stringByAppendingString:[_config objectForKey:kConfig_AppLatestVersion]]];
            default:
                break;
        }
        
        [_dictConfigSettings writeToFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameConfigSettings] atomically:NO];
        
        _alertNewAppVersion = nil;
    }
}

#pragma mark - Connection delegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == _urlConnGetCleverNetAdInfo)
    {
        [_dataReceivedCleverNetAd appendData:data];
    }
//    else if (connection == _urlConnGetUDID)
//        [_dataReceivedUDID appendData:data];
    else
        [_dataReceived appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //NSLog(error.descript);
//    if (connection == _urlConnGetUDID)
//	{
//        SAFE_RELEASE(_urlConnGetUDID);
//        SAFE_RELEASE(_dataReceivedUDID);
//	}
    if (connection == _urlConnGetCleverNetAdInfo)
	{
        SAFE_RELEASE(_urlConnGetCleverNetAdInfo);
        SAFE_RELEASE(_dataReceivedCleverNetAd);
        
        [self didGetCleverNETAdInfoFailed];
	}
    else if (connection == _urlConnGetNotifications)
    {
        SAFE_RELEASE(_urlConnGetNotifications);
        SAFE_RELEASE(_dataReceived);
    }    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    if (connection == _urlConnGetUDID)
//	{
//        SAFE_RELEASE(_urlConnGetUDID);
//        //NSError* theError = nil;
//        //NSDictionary* dictReturn = [[CJSONDeserializer deserializer] deserialize:_dataReceivedUDID error:&theError];
//        NSError* theError = nil;
//        NSDictionary* dictReturn = [[CJSONDeserializer deserializer] deserializeAsDictionary:_dataReceivedUDID error:&theError];
//        NSLog(theError.description);
//        
//        SAFE_RELEASE(_dataReceivedUDID);
//        
//        if ([[dictReturn objectForKey:kGetUDIDData_Result] isEqualToString:@"success"])
//        {
//            [_dictConfigSettings setObject:[dictReturn objectForKey:kGetUDIDData_UDID] forKey:keyUDID];
//            [_dictConfigSettings writeToFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameConfigSettings] atomically:NO];
//            
//            [self getNotification];
//        }
//        
//    }
//    else
    if (connection == _urlConnGetCleverNetAdInfo)
    {
        SAFE_RELEASE(_urlConnGetCleverNetAdInfo);
        NSError* theError = nil;
        NSDictionary* dictReturn = [[CJSONDeserializer deserializer] deserialize:_dataReceivedCleverNetAd error:&theError];
        NSString *strData = [[NSString alloc] initWithData:_dataReceivedCleverNetAd encoding:NSUTF8StringEncoding];
        //NSLog(@"%@",strData);
        SAFE_RELEASE(_dataReceivedCleverNetAd);
        
        [_dictLastCleverNetAdInfo removeAllObjects];
        
        if (dictReturn && [dictReturn respondsToSelector:@selector(objectForKey:)])
        {
            [_dictLastCleverNetAdInfo addEntriesFromDictionary:dictReturn];
            
            int nNumberParamCounter = 0;
            if ([dictReturn objectForKey:kCleverNetAdCachedInfo_Title] && [dictReturn objectForKey:kCleverNetAdCachedInfo_Title] != [NSNull null] && [[dictReturn objectForKey:kCleverNetAdCachedInfo_Title] length] > 0)
            {
                nNumberParamCounter++;
                [_dictCleverNetAdCachedInfo setObject:[dictReturn objectForKey:kCleverNetAdCachedInfo_Title] forKey:kCleverNetAdCachedInfo_Title];
            }
            
            if ([dictReturn objectForKey:kCleverNetAdCachedInfo_Content] && [dictReturn objectForKey:kCleverNetAdCachedInfo_Content] != [NSNull null] && [[dictReturn objectForKey:kCleverNetAdCachedInfo_Content] length] > 0)
            {
                nNumberParamCounter++;
                [_dictCleverNetAdCachedInfo setObject:[dictReturn objectForKey:kCleverNetAdCachedInfo_Content] forKey:kCleverNetAdCachedInfo_Content];
            }
            
            if ([dictReturn objectForKey:kCleverNetAdCachedInfo_Link] && [dictReturn objectForKey:kCleverNetAdCachedInfo_Link] != [NSNull null] && [[dictReturn objectForKey:kCleverNetAdCachedInfo_Link] length] > 0)
            {
                nNumberParamCounter++;
                [_dictCleverNetAdCachedInfo setObject:[dictReturn objectForKey:kCleverNetAdCachedInfo_Link] forKey:kCleverNetAdCachedInfo_Link];
            }
            
            // Assume that cleverNet Ad is valid if only client received all 3 params: title, content, link
            if (nNumberParamCounter > 2)
            {
                [_dictCleverNetAdCachedInfo setObject:[NSNumber numberWithBool:YES] forKey:kCleverNetAdInfo_Ready];
                [self didGetCleverNETAdInfoSuccessfully];
            }
            else
            {
                [_dictCleverNetAdCachedInfo setObject:[NSNumber numberWithBool:NO] forKey:kCleverNetAdInfo_Ready];
                [self didGetCleverNETAdInfoFailed];
            }
                
        }
        else
        {
            [_dictCleverNetAdCachedInfo setObject:[NSNumber numberWithBool:NO] forKey:kCleverNetAdInfo_Ready];
            [self didGetCleverNETAdInfoFailed];
        }
    }
	else if (connection == _urlConnGetNotifications)
    {
        SAFE_RELEASE(_urlConnGetNotifications);
        NSError* theError = nil;
        NSDictionary *dictReturn = [[[CJSONDeserializer deserializer] deserializeAsDictionary:_dataReceived error:&theError] retain];
//        NSLog(theError.description);
        SAFE_RELEASE(_dataReceived);
        
        if (dictReturn && ![_dictConfigNotifications isEqualToDictionary:dictReturn])
        {
            [_dictConfigNotifications setDictionary:dictReturn];
            if ([_dictConfigNotifications writeToFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameNotificationData] atomically:YES])
            {
                NSLog(@"CONFIG_NOTIFICATION write to file %@ OK",fileNameNotificationData);
                NSLog(@"%@",_dictConfigNotifications);
            }
            else
            {
                NSLog(@"CONFIG_NOTIFICATION write to file %@ FAILED",fileNameNotificationData);
            }
            
            _notifications = [_dictConfigNotifications objectForKey:kNotifications];
            _config        = [_dictConfigNotifications objectForKey:kConfig];
            
        // Check if current version is the last version on server, otherwise prevent app from show popup ads. It mean the popup ads won't show when Apple InReview time
            
            NSString* sAppLatestVersion = [_config objectForKey:kConfig_AppLatestVersion];
            
            if ([self isNewVersionInTestOrReviewTime])
                _notifications = nil;
            
        // Update popup ads rate
            if (_nAdRate <= 0)
                _nAdRate = [self getAdRate];
            
            /* Thong bao la da download xong file config */
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyDidLoadNewConfigNotification object:nil];
            
        // Background download ad images
            [NSThread detachNewThreadSelector:@selector(backgroundDownloadAppAdImages) toTarget:self withObject:nil];
            
        // Check & notify for new app version app update
        /*
            NSString *sCurProductVersion = [NSString stringWithFormat:@"%@",[self getProductVersion]];
            sAppLatestVersion = [_config objectForKey:kConfig_AppLatestVersion];
            
            if ([sCurProductVersion compare:sAppLatestVersion] == NSOrderedAscending)
            {
                NSNumber *numRemindUpdate = [_dictConfigSettings objectForKey:[kRemindUpdateNewAppVersion stringByAppendingString:sAppLatestVersion]];
                
               
                
                if (!numRemindUpdate || [numRemindUpdate boolValue])
                {
                    if ([delegate respondsToSelector:@selector(VDConfigNotification:notifyUpdateNewAppVersion:)])
                    {
                        [delegate VDConfigNotification:self notifyUpdateNewAppVersion:sAppLatestVersion];
                    }
                    else
                    {
                        NSString *strUpdateMessage = @"The new version is available on App Store. Please update for better experience. Thank you for using our product.";
                        NSString *strUpdate = @"Update now";
                        NSString *strRemindLater = @"Remind me later";
                        NSString *strCancel = @"Cancel";
                        
                        _alertNewAppVersion = [[UIAlertView alloc] initWithTitle:[self getProductName] message:strUpdateMessage delegate:self cancelButtonTitle:strCancel otherButtonTitles:strUpdate, strRemindLater, nil];
                        
                        [_alertNewAppVersion show];
                        [_alertNewAppVersion release];

                    }
                    
                }
            }
        */
            
        // Check for showing message to customer
            if ([[_config objectForKey:kConfig_MessageEnable] boolValue] && [[_config objectForKey:kConfig_MessageVersion] intValue] > [[_dictConfigSettings objectForKey:kConfig_MessageVersion] intValue])
            {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:[self getProductName] message:[_config objectForKey:kConfig_Message] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertMsg show];
                [alertMsg release];
                
                [_dictConfigSettings setObject:[_config objectForKey:kConfig_MessageVersion] forKey:kConfig_MessageVersion];
                [_dictConfigSettings writeToFile:[NSString stringWithFormat:@"%@/%@", appDataDirectoryPath, fileNameConfigSettings] atomically:NO];
            }
        }
        
        [dictReturn release];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == _urlConnGetCleverNetAdInfo)
    {
        [_dataReceivedCleverNetAd setLength:0];
    }
//    else if (connection == _urlConnGetUDID)
//        [_dataReceivedUDID setLength:0];
    else
        [_dataReceived setLength:0];
}

#pragma mark - Download AdImage(AdIcon), AdBanner, AdRectangle, AdFullscreen
- (void)setEnableAutoDownloadAllImages:(BOOL)bEnable
{
    _bIsAutoDownloadAllAdImages = bEnable;
}

- (void) backgroundDownloadAppAdImages
{
    if (!_bIsAutoDownloadAllAdImages)
        return;
    
    NSMutableDictionary *dictApp = [[NSMutableDictionary alloc] init];
    NSString *strImage = nil;
    NSString *strImageURL = nil;
    BOOL bIconDownload;
    NSString *sCurAdImagesDirectory = nil;
    
    for (int index = 0; index < [_notifications count]; index++)
    {
        [dictApp setDictionary:[_notifications objectAtIndex:index]];
        sCurAdImagesDirectory = [appAdImagesDirectoryPath stringByAppendingPathComponent:[dictApp objectForKey:kNotification_ID]];
        
    // AdIcon
        strImage = [dictApp objectForKey:kNotification_AdImage];
        if (strImage && strImage != (id)[NSNull null] && strImage.length > 0)
        {
            if ([strImage rangeOfString:@"http://"].location == 0)
                strImageURL = strImage;
            else
                strImageURL = [NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,strImage];
            
            bIconDownload = [VDConfigNotification downLoadFileFromURL:strImageURL toFolderPath:sCurAdImagesDirectory withFileName:[strImage lastPathComponent]:NO];
        }
        
    // AdBanner
        strImage = [dictApp objectForKey:kNotification_AdBanner];
        if (strImage && strImage != (id)[NSNull null] && strImage.length > 0)
        {
            if ([strImage rangeOfString:@"http://"].location == 0)
                strImageURL = strImage;
            else
                strImageURL = [NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,strImage];
            
            bIconDownload = [VDConfigNotification downLoadFileFromURL:strImageURL toFolderPath:sCurAdImagesDirectory withFileName:[strImage lastPathComponent] :NO];
        }
        
    // AdRectangle
        strImage = [dictApp objectForKey:kNotification_AdRectangle];
        if (strImage && strImage != (id)[NSNull null] && strImage.length > 0)
        {
            if ([strImage rangeOfString:@"http://"].location == 0)
                strImageURL = strImage;
            else
                strImageURL = [NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,strImage];
            
            bIconDownload = [VDConfigNotification downLoadFileFromURL:strImageURL toFolderPath:sCurAdImagesDirectory withFileName:[strImage lastPathComponent] :NO];
        }
        
    //AdFullscreen
        strImage = [dictApp objectForKey:kNotification_AdFullscreen];
        if (strImage && strImage != (id)[NSNull null] && strImage.length > 0)
        {
            if ([strImage rangeOfString:@"http://"].location == 0)
                strImageURL = strImage;
            else
                strImageURL = [NSString stringWithFormat:@"%@/%@",urlDownloadAppIconOnServer,strImage];
            
            bIconDownload = [VDConfigNotification downLoadFileFromURL:strImageURL toFolderPath:sCurAdImagesDirectory withFileName:[strImage lastPathComponent] :NO];
        }
        
    }
    
    [dictApp release];
    [self performSelectorOnMainThread:@selector(mainThreadDidFinishDownloadAppIcon) withObject:nil waitUntilDone:YES];
}

- (void) mainThreadDidFinishDownloadAppIcon
{
    //[[NSNotificationCenter defaultCenter] postNotificationName:kNotification_DidLoadNewConfig object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyDidFinishDownloadAdImages object:nil];
}


/** Kiểm tra xem hiện tại app có đang trong giai đoạn được Free Ad hay không? Nếu có thì tất cả các loại quảng cáo phải tắt
 * @return YES/NO = in free/not free ad time
 **/
- (BOOL)isInAdFreeTime
{
    return [self getNumberOfAdFreeHoursRemain] > 0;
}

/** Trả về số giờ free quảng cáo còn lại
 */
- (int)getNumberOfAdFreeHoursRemain
{
    NSDate* expireDateAdFree = [[NSUserDefaults standardUserDefaults] objectForKey:keyExpiredDateAdFree];
    if (!expireDateAdFree)
        return 0;

    NSTimeInterval remainTimeInterval = [expireDateAdFree timeIntervalSinceDate:[VDConfigNotification getCurrentDate]];
    
    int nResult = floor((remainTimeInterval+60*60)/(60*60));
    if (nResult < 0) nResult = 0;
    
    int nNumberOfAdFreeHoursRemain = [[[NSUserDefaults standardUserDefaults] objectForKey:keyNumberOfAdFreeHoursRemain] intValue];
    
    if (nResult >= nNumberOfAdFreeHoursRemain)  // User đã chỉnh thời gian, set lại với mốc current date
    {
        expireDateAdFree = [[VDConfigNotification getCurrentDate] dateByAddingTimeInterval:(NSTimeInterval)(nNumberOfAdFreeHoursRemain*60*60)];
        [[NSUserDefaults standardUserDefaults] setObject:expireDateAdFree forKey:keyExpiredDateAdFree];

        nResult = nNumberOfAdFreeHoursRemain;
    }
    else // Cập nhật lại số giờ free ad
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:nResult] forKey:keyNumberOfAdFreeHoursRemain];
    }

    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return nResult;
}

- (void)addAdFreeHours:(int)nHours
{
    NSDate* curAdFreeExpiredDate = [[NSUserDefaults standardUserDefaults] objectForKey:keyExpiredDateAdFree];
    if (!curAdFreeExpiredDate)
        curAdFreeExpiredDate = [VDConfigNotification getCurrentDate];
    
    int nNumberOfAdFreeHoursRemain = [[[NSUserDefaults standardUserDefaults] objectForKey:keyNumberOfAdFreeHoursRemain] intValue];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:(nNumberOfAdFreeHoursRemain + nHours)] forKey:keyNumberOfAdFreeHoursRemain];
    
    NSDate* newExpireDateAdFree = [curAdFreeExpiredDate dateByAddingTimeInterval:(NSTimeInterval)(nHours*60*60)];
    [[NSUserDefaults standardUserDefaults] setObject:newExpireDateAdFree forKey:keyExpiredDateAdFree];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)dealloc
{
    SAFE_RELEASE(_dictConfigNotifications);
    SAFE_RELEASE(_dictAdImpressionCount);
    SAFE_RELEASE(_dictAdClickCount);
    SAFE_RELEASE(_dictConfigSettings);
    
    SAFE_RELEASE(_dictCleverNetAdCachedInfo);
    SAFE_RELEASE(_dictLastCleverNetAdInfo);
    
    SAFE_RELEASE(_strCleverNetAdZoneID);
    SAFE_RELEASE(sTestModeAppID);
    SAFE_RELEASE(sTestModeAppVersion);
    
    _notifications = nil;
    _config = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

@end
