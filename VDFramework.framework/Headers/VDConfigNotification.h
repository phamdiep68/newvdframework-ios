//
//  VDConfigNotification.h
//  PPCLINK
//
//  Created by Do Lam on 10/8/12.
//
//

#define BUILD_VERSION      @"1.8.18"
/****************************************************************
* Last updated: 2015.08.31
*
*****************************************************************/
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifdef DEBUG
#   define NSLog(...) NSLog(__VA_ARGS__);
#else
#   define NSLog(...)
#endif

//#define SERVER_CONFIGNOTIFICATION   @"deltago.com"
//#define SERVER_CHANNEL_DEFAULT      @"mdcgate.com"

//#define keyUDID                     @"keyUDID"
#define kCurrentIndexAd             @"currentIndexAd"
#define kCurrentIndexAdHighPriority @"currentIndexAdHighPriority"
#define kCountOpenApp               @"countOpenApp"
#define kRemindUpdateNewAppVersion  @"remindUpdateNewAppVersion"

#define kGetUDIDData_Result     @"result"
#define kGetUDIDData_UDID       @"udid"

#define kConfig                                     @"config"
#define kConfig_EnableAds                           @"Ads"
#define kConfig_AdsType                             @"Ads_type"
#define kConfig_FreeAdsRate                         @"FreeAdsRate"
#define kConfig_MinFreeAdsRate                      @"MinFreeAdsRate"
#define kConfig_PaidAdsRate                         @"PaidAdsRate"
#define kConfig_MinPaidAdsRate                      @"MinPaidAdsRate"
#define kConfig_AppLatestVersion                    @"app_latest_version"
#define kConfig_MessageVersion                      @"message_version"
#define kConfig_Message                             @"message"
#define kConfig_MessageEnable                       @"message_enable"
#define kConfig_ShowAdWhenSwitchChannel               @"showAdWhenSwitchChannel"
#define kConfig_ServerChannel                           @"serverChannel"
#define kConfig_AppstoreURL                             @"appstoreurl"
#define kConfig_ContactUsUIStyle                        @"contactUsUIStyle"
#define kConfig_AdRectangleRefreshRate                  @"adRectangleRefreshRate"
#define kConfig_CostOneDayFreeAd                        @"CostOneDayFreeAd"  // Cost (in points) per ad-free day/hours
#define kConfig_PercentVideoInterstitalAd               @"PercentVideoInterstitalAd"
#define kConfig_ipServer                                @"ipServer"
#define kConfig_AdTestMode                              @"AdTestMode"


////////// KEY FOR PPCLINK Ad Network mediation //////
#define kConfig_PPCLINKAdsMediationBanner                @"PPCLINKMediationConfig_AdBanner"
#define kConfig_PPCLINKAdsMediationInterstitial           @"PPCLINKMediationConfig_Interstitial"
#define kConfig_PPCLINKAdsMediationVideoInterstitial       @"PPCLINKMediationConfig_VideoInterstitial"
#define kConfig_PPCLINKAdsMediationNativeAd                 @"PPCLINKMediationConfig_NativeAd"
// Khoang thoi gian toi thieu giua 2 lan hien quang cao interstitial
#define kConfig_InterstitialAdFreeMinTimeInterval                   @"fullScreenAdFreeBetweenAdShowMinTimeInterval"
// Khoang thoi gian toi thieu giua 2 lan hien quang cao interstitial khi da user click/xem quang cao full/video
#define kConfig_InterstitialAdFreeMinTimeIntervalBonus              @"fullScreenAdFreeTimeIntervalBonus"
// Khoang thoi gian toi thieu tu khi mo app den khi show interstitial ad dau tien
#define kConfig_InterstitialAdFreeWhenOpenAppMinTimeInterval        @"fullScreenAdFreeWhenOpenAppTimeInterval"
//Enable quay vong mang quang cao
#define kConfig_RecirculateAdNetworkEnable                          @"RecirculateAdNetworkEnable"
#define kConfig_InterstitialAdMaxTimePerSession                     @"InterstitialAdMaxTimePerSession"


#define kNotifications                  @"notifications"
#define kNotification_ID                @"NotificationId"
#define kNotification_Description       @"Description"
#define kNotification_Title             @"Title"
#define kNotification_TryNowText        @"TryNowText"
#define kNotification_CancelText        @"CancelText"
#define kNotification_MaxClick          @"MaxClick"
#define kNotifcation_MaxImpression      @"MaxImpression"
#define kNotification_URL               @"URL"
#define kNotification_Target            @"Target"
#define kNotification_AppId             @"AppId"
#define kNotification_AdImage           @"AdImage"
#define kNotification_AdBanner          @"AdBanner"
#define kNotification_AdRectangle       @"AdRectangle"
#define kNotification_AdFullscreen      @"AdFullscreen"
#define kNotification_AdIconMoreApps    @"AdIconMoreApps"
#define kNotification_AdOrder           @"order"

//#define kNotification_AdImageURL        @"AdImageURL"
#define kNotification_InstalledRewardPoints       @"installedRewardPoints"   // Number of points reward to user when they installed the app notification
#define kNotification_NumberOfAdFreeHours        @"NumberOfAdFreeHours"


#define kCleverNetAdCachedInfo_CurrentURL   @"link"
#define kCleverNetAdCachedInfo_Link         @"link"
#define kCleverNetAdCachedInfo_Content      @"content"
#define kCleverNetAdCachedInfo_Title        @"title"
#define kCleverNetAdInfo_Ready              @"isCleverNETReady"

// Post notification when update new config & notification
#define kNotifyDidLoadNewConfigNotification             @"notifyDidLoadNewConfigNotification"
// Post notification when Ad images (adbanner,adrectangle,..) of all notifications just finished!
#define kNotifyDidFinishDownloadAdImages         @"notifyDidFinishDownloadAdImages"
//#define kNotifyJustShowPopupAd                  @"notifyJustShowPopupAd"

#define kNotifyUserClick
#define appAdImagesDirectoryPath      [appDataDirectoryPath stringByAppendingString:@"/adimages"] 

#define CLEVERNET_AD_TEXT_ZONE_ID       @"7a266402ce2c1f100849c6f6c6a9b648"

typedef enum
{
    DT_IPHONE = 1,
    DT_IPAD = 2,
    DT_ANDROID_PHONE = 4,
    DT_ANDROID_TABLET = 8
} DEVICE_TARGET;

#define kNotification_Product           @"Product"
typedef enum
{
    PT_FREE = 1,
    PT_PAID = 2
} PRODUCT_TYPE;

#define kNotification_Type              @"Type"
typedef enum
{
    NTYPE_APP = 0,   // Hiện quảng cáo cho 1 app, hỗ trợ tải app để nhận thưởng
    NTYPE_INAPP = 1,  // Yêu cau user mua inapp
    NTYPE_CLEVERNET = 2, // CLEVERNET text ad
    NTYPE_VIEWADS = 3,   // Gợi ý user xem/click quảng cáo video/full để nhận thưởng
    NTYPE_MESSAGE = 4,  // Dùng để hiện các thông điệp, tin tức từ admin: có ưu tiên hiện cao nhất khi mở app mà ko quan tâm cấu hình số lần mở app
    NTYPE_APP_HIGH_PRIORITY = 5, // Thông tin giống loại NTYPE_APP nhưng có ưu tiên hiện cao hơn NTYPE_APP
    NTYPE_ALL = 88    // Hiện quay vòng các loại quảng cáo trả về
} NOTIFICATION_TYPE;

typedef enum
{
    ADIMAGETYPE_ICON = 0,
    ADIMAGETYPE_BANNER = 1,
    ADIMAGETYPE_RECTANGLE = 2,
    ADIMAGETYPE_FULLSCREEN = 3,
    ADIMAGE_TYPE_ICONMOREAPPS = 4
} ADIMAGE_TYPE;


@class VDConfigNotification;
extern VDConfigNotification* g_vdConfigNotification;

@protocol VDConfigNotificationDelegate <NSObject>

/* Được gọi khi nhận được yêu cầu hiện notification với các thông tin lưu trong dictNotifyInfo. Cần implement khi muốn hiện notification theo style riêng của app. Nếu không implement sẽ show notification theo cách mặc định (UIAlertView)
 */
- (void)VDConfigNotification:(VDConfigNotification *)vdConfigNotification showNotificationWithInfo:(NSDictionary *)dictNotifyInfo;

/* Được gọi (thường là khi mở app) khi kiểm tra thấy user đã cài đặt thành công một app để nhận thưởng (Ad-free)
*/
- (void)VDConfigNotification:(VDConfigNotification *)vdConfigNotification userDidInstallAppForRewardWithInfo:(NSDictionary *)dictAppInfo;

//@optional
//- (void)VDConfigNotification:(VDConfigNotification *)vdConfigNotification notifyUpdateNewAppVersion:(NSString*)sNewVersion;

@end

@interface VDConfigNotification : NSObject

@property(nonatomic, retain) NSDictionary   *_config;
@property(nonatomic, retain)    NSArray     *_notifications;
@property(nonatomic, assign) id<VDConfigNotificationDelegate> delegate;


/**
 * YES/NO <=> tự động/không tự động show popup ad (trừ admin message) theo cấu hình server khi mở app
 * Giá trị mặc định = YES
 */
@property (nonatomic, assign) BOOL  shouldAutoShowPopupAdWhenOpenApp;

- (id)initWithProductType:(PRODUCT_TYPE)productType;

- (id)initWithAppID:(NSString*)sAppID andAppVersion:(NSString*)sAppVersion;  // For test only

/** Show popup notification
 * @param notificationType
 * @param bForceShow: = YES/NO <=> Không/Có kiểm tra điều kiện số lần mở app trước khi show
 * @return YES/NO = Show/not show notification
 **/
- (BOOL)showPopUpNotification:(NOTIFICATION_TYPE)notificationType forceShow:(BOOL)bForceShow;

/** Kiểm tra xem hiện tại app có đang trong giai đoạn được thưởng Free Ad hay không? Nếu có thì tất cả các loại quảng cáo sẽ không nên hiện trừ một số TH đặc biệt.
 * Cần gọi hàm này để kiểm tra ở mỗi thời điểm chuẩn bị hiện quảng cáo
 * @return YES/NO = in free/not free ad time
 **/
- (BOOL)isInAdFreeTime;

/** Trả về số giờ free quảng cáo còn lại
 */
- (int)getNumberOfAdFreeHoursRemain;

/** Cộng thêm cho user nHours giờ Ad free
 */
- (void)addAdFreeHours:(int)nHours;

/**
 *  Khi implement delegate và sử dụng Custom AlertView để show notification, khi người dùng click View/Try Now phải gọi hàm này để ghi nhận và thống kê.
 * @param sNotificationID
 * @param bYesNo: = YES/NO nếu user click để có/không được nhận thưởng
 */
- (void)userJustClickTryNowWithNotificationId:(NSString *)sNotificationID forReward:(BOOL)bYesNo;

/**
 *  Tương tự hàm userJustClickTryNowWithNotificationId:(NSString *)sNotificationID forReward:(BOOL)bYesNo nhưng
 * với tham số bYesNo = NO
 */
- (void)userJustClickTryNowWithNotificationId:(NSString *)sNotificationID;

// Khi user click vao banner hay nut TryNow cua popup, goi ham nay de thong ke
- (void)userJustClickTryNowWithNotificationId:(NSString *)sNotificationID adImageType:(ADIMAGE_TYPE)adImageType;

// Khi quang cao popup/banner duoc hien, goi ham nay de thong ke
- (void)justShowNotificationWithID:(NSString *)sNotificationID adImageType:(ADIMAGE_TYPE)adImageType;

// Set lại loại app là bản Free hay bản Pro
- (void)setProductType:(PRODUCT_TYPE)productType;  // Paid/Free

/* Lấy danh sách các notification có hỗ trợ loại quảng cáo kiểu Icon/Banner/Rectangle/Fullscreen.
 @param adImageType: loại quảng cáo hình ảnh
 @param bIncludeInstalledApps: =YES/NO: nếu có/không bao gồm cả app đã cài
 @ @return Trả về một danh sách các notifications có hỗ trợ quảng cáo kiểu adImageType, mỗi notification ngoài các thông tin dành cho quảng cáo popup dạng text sẽ có thêm trường URL hình ảnh quảng cáo với key là AdImage/AdBanner/AdRectangle/AdFullscreen/AdIconMoreApps tương ứng với dạng Icon/Banner/Rectangle/Fullscreen/AdIconMoreApps
*/
- (NSArray*)getListOfNotificationWithAdImageType:(ADIMAGE_TYPE)adImageType includeInstalledApps:(BOOL)bIncludeInstalledApps;

// Như hàm trên với tham số bIncludeInstalledApps = NO
- (NSArray*)getListOfNotificationWithAdImageType:(ADIMAGE_TYPE)adImageType;

- (NSArray*)getListOfNotificationForMoreApps:(BOOL)bIncludeInstalledApps;

/*
 @ @return YES/NO: nếu đủ/không điều kiện để show notification (theo config rate trong khoảng [MinFreeAdRate, FreeAdRate]
 */
- (BOOL)isOnTimeToShowANotification;
/*
 * @param notificationType: loại notification
 @ @return Trả về thông tin của 1 notification đủ điều kiện hiển thị
 */
- (NSDictionary*)getANotificationInfoToShow:(NOTIFICATION_TYPE)notificationType;

/*
 @ @return Trả về thông tin một quảng cáo PPCLINK Native Ad: hien tai tuong ung la 1 noification
 */
- (NSDictionary*)getAPPCLINKNativeAd;


- (void)setEnableAutoDownloadAllImages:(BOOL)bEnable;

- (void) setCleverNetAdZoneID:(NSString*)zoneID;

// PPCLINK Reward module
- (NSArray*)getListOfAppDownloadForReward;

// Kiem tra xem đây có phải phiên bản mới nhất và đang trong thời gian Test hoặc chờ Review không
- (BOOL)isNewVersionInTestOrReviewTime;

// Utilities functions
+ (NSString*)getProductOnAppStoreURL;
+ (NSString*)getItunesApplicationID;
+ (NSString*) getProductBuildVersion;
+ (int)getRandomIntValue:(int)nN;
+ (BOOL)isInstalledAppId:(NSString *)sAppId;
// Hàm lấy ngày hiện tại và đảm bảo trả về ngày hiện tại luôn lớn hơn ngày hiện tại đã lưu trước đó: tránh TH user lùi ngày
+ (NSDate*)getCurrentDate;

- (NSString*) getProductName;
- (NSString*) getProductVersion;
- (NSString*)getBundleID;

// Set kiểu popup: từ version 1.7 hàm này sẽ bị loại bỏ và khi muốn customize popup
// cần implement hàm trong VDConfigNotificationDelegate
//- (void) setPopupStyle:(POPUP_STYLE)style __attribute__((deprecated));

@end

